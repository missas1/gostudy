package main

import (
	"fmt"

	"example.com/greetings"

	"log"
)

func main() {

	log.SetPrefix("greetings: ")
	log.SetFlags(0)

	// Request a greeting message
	//message, err := greetings.Hello("Gladys")


	// A slice of names
	names := []string{"Gladys", "Samantha", "Darrin"}

	// Request greeting messages for the names.
	messages, err := greetings.Hellos(names)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(messages)
	// fmt.Println("Hello, World!")
	// fmt.Println(quote.Go())
	// message := greetings.Hello("Gladys")
	// fmt.Println(message)
}
